package studio2;

import cse131.ArgsProcessor;

public class Gamblersruin {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArgsProcessor ap = new ArgsProcessor(args);
		int startAmount = ap.nextInt("start amount dollars?");
		//The amount of money that you start with
		double winChance = ap.nextDouble("Win Chance?");
		//The probability that you win a gamble
		int winAmount = ap.nextInt("Win Amount?");
		//If you reach this amount of money, then you won
		int totalPlays = ap.nextInt("How many plays?");
		//The number of times you simulate the problem

		//double lossChance = 1 - winChance;
			double Ruin = 0;
		//double a = lossChance / winChance;
		//	double b = 1 - lossChance / winChance;

		int ruincount = 0;
		//int wincount = 0;
/*
		if (lossChance != winChance)
			Ruin = (Math.pow(a, startAmount)) - (Math.pow(a, winAmount)) / (Math.pow(b, winAmount)); 

		if (lossChance == winChance) 
		*/	
		Ruin = 1 - ((float) startAmount / winAmount);

		for(int i = 1; i <= totalPlays; i++) {
			int rounds = 0;

			int dollars = startAmount;
			while (dollars > 0 && dollars < winAmount) {
				rounds ++;
				double randomSpin = Math.random();
				if (randomSpin <= winChance) dollars ++;

				else dollars --; 	

			}
			//if (dollars == 0) ruincount ++; System.out.println("Simulation " + i +": " + rounds + " rounds" + " Loss");

			if (dollars == winAmount) { 
				//wincount ++; 
				System.out.println("Simulation " + i +": " + rounds + " rounds" + " Win");
			}

			else {
				System.out.println("Simulation " + i +": " + rounds + " rounds" + " Loss");
				ruincount ++;
			}

		}
		double ARR = (float) ruincount / totalPlays;
		System.out.println("Losses: " + ruincount +" Simulations: " + totalPlays );
		System.out.print("Actual Ruin Rate: " + ARR + " Expected Ruin Rate: " + Ruin );

	}

}



