package lab1;
import cse131.ArgsProcessor;

public class Nutrition {
	

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		ArgsProcessor ap = new ArgsProcessor(args);
		/*
		name	The name of this food, provided as a String
		carbs	The number of grams of carbohydrates in this food
		fat	The number of grams of fat in this food
		protein	The number of grams of protein in this food
		statedCals

		*/
		String name = ap.nextString("What is the name of the food?");
		double carbs = ap.nextDouble("How many grams of carbs?");
		double fat = ap.nextDouble("How many grams of fat?");
		double protein = ap.nextDouble("How many grams of protein?");
		int statedCals = ap.nextInt("How many calories?");
		
		double availableCal = (protein * 4 + carbs * 4 + fat * 9);

		double unavailableCal =  100 * availableCal - 100 * statedCals;
		
		double fiber  = (unavailableCal / 100) / .04;
		
		double fiberr =  Math.round(fiber);
		double UCal =  Math.round(unavailableCal);

		
		int CarbPercent = (int) (carbs * 4 * 1000 / statedCals);
		double carbp =	Math.round(CarbPercent);
		
		 int fatPercent = (int) (fat * 9 * 1000 / statedCals);
		 double fatp =	 Math.round(fatPercent);
		
		int proteinPercent = (int) (protein * 4 * 1000 / statedCals);
		double proteinp =  Math.round(proteinPercent);
	
		boolean lowC = carbs * 4 <= statedCals * .25;
		boolean lowf = fat * 9 <= statedCals * .15;
		double headschance = Math.random();
		boolean heads = headschance > .5;
		
		
	    System.out.println("This food is said to have " + statedCals + " (available) Calories.");
	    System.out.println("With " + UCal / 100 + " unavailable Calories, this food has " + fiberr / 100 + " grams of fiber");

	    System.out.println("Approximately");
	    System.out.println( carbp / 10 + " percent of your food is carbohydrates");
	    System.out.println( fatp / 10 + " percent of your food is fat");
	    System.out.println( proteinp / 10 + " percent of your food is protein");
	    System.out.println("This food is acceptable for a low-carb diet? " + lowC);
	    System.out.println("This food is acceptable for a low-fat diet?  " + lowf);
	    System.out.println("By coin flip, you should eat this food? " + heads);
		



	}

}
